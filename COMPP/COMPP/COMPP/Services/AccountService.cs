using COMPP.Models;
using COMPP.Models.AddProfile;
using COMPP.Models.EditProfile;
using COMPP.Models.Login;
using COMPP.Models.Register;
using COMPP.Models.EditProfile;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using System.Collections.ObjectModel;

namespace COMPP.Services
{
    internal class AccountService : IAccountService
    {
        private static AccountService _ServiceClientInstance;
        public static AccountService ServiceClientInstance
        {
            get
            {
                if (_ServiceClientInstance == null)
                    _ServiceClientInstance = new AccountService();
                return _ServiceClientInstance;
            }
        }

        private JsonSerializer _serializer = new JsonSerializer();
        private HttpClient _httpClient;

        // This method must be in a class in a platform project, even if
        // the HttpClient object is constructed in a shared project.
        public HttpClientHandler GetInsecureHandler()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
            {
                if (cert.Issuer.Equals("CN=localhost"))
                    return true;
                return errors == System.Net.Security.SslPolicyErrors.None;
            };
            return handler;
        }

        public AccountService()
        {
#if DEBUG
            HttpClientHandler insecureHandler = GetInsecureHandler();
            _httpClient = new HttpClient(insecureHandler);
#else
    HttpClient client = new HttpClient();
#endif
            _httpClient.BaseAddress = new Uri("http://compp-api.eastasia.cloudapp.azure.com/api/");
            _httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var token = Task.Run(() => SecureStorage.GetAsync("token")).Result;
            if(token != null)
            {
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
        }

        public async Task<LoginResponseModel> Login(string email, string password)
        {
            try
            {
                LoginRequestModel loginRequestModel = new LoginRequestModel()
                {
                    password = password,
                    email = email
                };

                var content = new StringContent(JsonConvert.SerializeObject(loginRequestModel), Encoding.UTF8, "application/json");
                var response = await _httpClient.PostAsync("Auth/login", content);
                response.EnsureSuccessStatusCode();

                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<LoginResponseModel>(json);
                    // Store token in secure storage
                    try
                    {
                        await SecureStorage.SetAsync("token", jsonContent.Token);
                        _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jsonContent.Token);
                    }
                    catch (Exception ex)
                    {
                        // Possible that device doesn't support secure storage on device
                    }

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<RegisterResponseModel> Register(string email, string name, string password)
        {
            try
            {
                RegisterRequestModel registerRequestModel = new RegisterRequestModel()
                {
                    name = name,
                    password = password,
                    email = email
                };

                var content = new StringContent(JsonConvert.SerializeObject(registerRequestModel), Encoding.UTF8, "application/json");  
                var response = await _httpClient.PostAsync("Auth/register", content);
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<RegisterResponseModel>(json);

                    return jsonContent;
                }

            }   
            catch (Exception ex)
            {
                
                return null;
            }
        }

        public async Task<object> AddProfile(int id, AddProfileRequestModel profileInfo)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(profileInfo), Encoding.UTF8, "application/json");
                var response = await _httpClient.PutAsync(string.Format("Accounts/{0}/addprofile", id), content);
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<RegisterResponseModel>(json);

                    return jsonContent;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<object> EditProfileIdentity(int id, EditProfileRequestModel profileInfo)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(profileInfo), Encoding.UTF8, "application/json");
                var response = await _httpClient.PutAsync(string.Format("Accounts/{0}", id), content);
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<EditProfileRequestModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<ProfileResponseModel> GetProfile(int id)
        {
            try
            {
                var response = await _httpClient.GetAsync(string.Format("Profile/{0}", id));
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<ProfileResponseModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<EducationResponseModel> GetEducations(int id)
        {
            try
            {
                var response = await _httpClient.GetAsync(string.Format("AccountEducations/{0}", id));
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<EducationResponseModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<ExperienceResponseModel> GetExperiences(int id)
        {
            try
            {
                var response = await _httpClient.GetAsync(string.Format("AccountExperiences/{0}", id));
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<ExperienceResponseModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<object> EditExperience(int id, ExperienceRequestModel experienceInfo)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(experienceInfo), Encoding.UTF8, "application/json");
                var response = await _httpClient.PutAsync(string.Format("AccountExperiences/{0}", id), content);
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<ExperienceRequestModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<object> AddExperience(ExperienceRequestModel experienceInfo)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(experienceInfo), Encoding.UTF8, "application/json");
                var response = await _httpClient.PostAsync(string.Format("AccountExperiences"), content);
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<ExperienceRequestModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<object> DeleteExperience(int id)
        {
            try
            {
                var response = await _httpClient.DeleteAsync(string.Format("AccountExperiences/{0}", id));
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<ExperienceRequestModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<object> DeleteEducation(int id)
        {
            try
            {
                var response = await _httpClient.DeleteAsync(string.Format("AccountEducations/{0}", id));
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<ExperienceRequestModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<object> EditEducation(int id, EducationRequestModel educationInfo)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(educationInfo), Encoding.UTF8, "application/json");
                var response = await _httpClient.PutAsync(string.Format("AccountEducations/{0}", id), content);
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<EducationRequestModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<object> AddEducation(EducationRequestModel educationInfo)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(educationInfo), Encoding.UTF8, "application/json");
                var response = await _httpClient.PostAsync(string.Format("AccountEducations"), content);
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<EducationRequestModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        public async Task DeleteAccount(int id)
        {
            SecureStorage.RemoveAll();
            try
            {
                var response = await _httpClient.DeleteAsync(string.Format("Accounts/{0}", id));
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                 _httpClient.DefaultRequestHeaders.Remove("Authorization");
            }
            catch (Exception ex)
            {
            }
        }

        public void Logout()
        {
            SecureStorage.RemoveAll();
            _httpClient.DefaultRequestHeaders.Remove("Authorization");
        }


        public Task Delete()
        {
            return null;
        }

        public async Task<object> EditAbout(int id, EditProfileAboutRequestModel profileInfo)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(profileInfo), Encoding.UTF8, "application/json");
                var response = await _httpClient.PutAsync(string.Format("Accounts/{0}", id), content);
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<EditProfileAboutRequestModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<AccountModel> GetAccountInfo(int id)
        {
            var response = await _httpClient.GetAsync($"Accounts/{id}");
            var content = response.Content.ReadAsStringAsync();
            response.EnsureSuccessStatusCode();
            using (var stream = await response.Content.ReadAsStreamAsync())
            using (var reader = new StreamReader(stream))
            using (var json = new JsonTextReader(reader))
            {
                var account = _serializer.Deserialize<AccountModel>(json);

                return account;
            }
        }


    }
}
