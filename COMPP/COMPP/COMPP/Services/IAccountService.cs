﻿using COMPP.Models;
using COMPP.Models.Login;
using COMPP.Models.Register;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace COMPP.Services
{
    internal interface IAccountService
    {
        Task<LoginResponseModel> Login(string email, string password);

        Task<RegisterResponseModel> Register(string email, string name, string password);

        Task Delete();

        Task<AccountModel> GetAccountInfo(int id);
    }
}
