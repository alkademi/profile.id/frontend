﻿using COMPP.Models.Search;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace COMPP.Services
{
    internal class SearchService
    {
        private static SearchService _ServiceClientInstance;
        public static SearchService ServiceClientInstance
        {
            get
            {
                if (_ServiceClientInstance == null)
                    _ServiceClientInstance = new SearchService();
                return _ServiceClientInstance;
            }
        }

        private JsonSerializer _serializer = new JsonSerializer();
        private HttpClient _httpClient;

        // This method must be in a class in a platform project, even if
        // the HttpClient object is constructed in a shared project.
        public HttpClientHandler GetInsecureHandler()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
            {
                if (cert.Issuer.Equals("CN=localhost"))
                    return true;
                return errors == System.Net.Security.SslPolicyErrors.None;
            };
            return handler;
        }

        public SearchService()
        {
#if DEBUG
            HttpClientHandler insecureHandler = GetInsecureHandler();
            _httpClient = new HttpClient(insecureHandler);
#else
    HttpClient client = new HttpClient();
#endif
            _httpClient.BaseAddress = new Uri("http://compp-api.eastasia.cloudapp.azure.com/api/");
            _httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var token = Task.Run(() => SecureStorage.GetAsync("token")).Result;
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

        public async Task<List<SearchResponseModel>> SearchByName(string query)
        {
            try
            {
                var response = await _httpClient.GetAsync($"Accounts?name={query}");
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var searchResult = _serializer.Deserialize<List<SearchResponseModel>>(json);

                    return searchResult;
                }
            }
            catch
            {
                return null;
            }
        }

        public async Task<List<SearchResponseModel>> SearchByJob(string query)
        {
            try
            {
                var response = await _httpClient.GetAsync($"Accounts?job={query}");
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var searchResult = _serializer.Deserialize<List<SearchResponseModel>>(json);

                    return searchResult;
                }
            }
            catch
            {
                return null;
            }
        }
        public async Task<List<SearchResponseModel>> SearchByCompany(string query)
        {
            try
            {
                var response = await _httpClient.GetAsync($"Accounts?company={query}");
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var searchResult = _serializer.Deserialize<List<SearchResponseModel>>(json);

                    return searchResult;
                }
            }
            catch
            {
                return null;
            }
        }
    }
}
