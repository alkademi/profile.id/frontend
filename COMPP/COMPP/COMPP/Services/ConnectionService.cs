﻿using COMPP.Models.Connection;
using COMPP.Models.Search;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace COMPP.Services
{
    internal class ConnectionService
    {
        private static ConnectionService _ServiceClientInstance;
        public static ConnectionService ServiceClientInstance
        {
            get
            {
                if (_ServiceClientInstance == null)
                    _ServiceClientInstance = new ConnectionService();
                return _ServiceClientInstance;
            }
        }

        private JsonSerializer _serializer = new JsonSerializer();
        private HttpClient _httpClient;

        // This method must be in a class in a platform project, even if
        // the HttpClient object is constructed in a shared project.
        public HttpClientHandler GetInsecureHandler()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
            {
                if (cert.Issuer.Equals("CN=localhost"))
                    return true;
                return errors == System.Net.Security.SslPolicyErrors.None;
            };
            return handler;
        }

        public ConnectionService()
        {
#if DEBUG
            HttpClientHandler insecureHandler = GetInsecureHandler();
            _httpClient = new HttpClient(insecureHandler);
#else
    HttpClient client = new HttpClient();
#endif
            _httpClient.BaseAddress = new Uri("http://compp-api.eastasia.cloudapp.azure.com/api/");
            _httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var token = Task.Run(() => SecureStorage.GetAsync("token")).Result;
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

        public async Task<List<ConnectionResponseModel>> GetConnectionList(int accountID)
        {
            try
            {
                var response = await _httpClient.GetAsync(string.Format("Accounts/{0}/connections", accountID));
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<List<ConnectionResponseModel>>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<PostConnectionResponseModel> CheckConnectionFromTo(int from, int to)
        {
            try
            {
                var response = await _httpClient.GetAsync(string.Format("Connections/check/{0}/{1}", from, to));
                var responseMessage = await response.Content.ReadAsStringAsync();
                response.EnsureSuccessStatusCode();

                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<PostConnectionResponseModel>(json);

                    return jsonContent;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<PostConnectionResponseModel> AddConnection(int from, int to)
        {
            try
            {
                PostConnectionRequestModel postConnectionRequestModel = new PostConnectionRequestModel()
                {
                    FromAccountID = from,
                    ToAccountID = to,
                };

                var content = new StringContent(JsonConvert.SerializeObject(postConnectionRequestModel), Encoding.UTF8, "application/json");
                var response = await _httpClient.PostAsync("Connections", content);
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var reader = new StreamReader(stream))
                using (var json = new JsonTextReader(reader))
                {
                    var jsonContent = _serializer.Deserialize<PostConnectionResponseModel>(json);

                    return jsonContent;
                }

            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public async Task<bool> DeleteConnection(int connectionID)
        {
            try
            {
                var response = await _httpClient.DeleteAsync(string.Format("Connections/{0}", connectionID));
                var responseMessage = response.Content;
                response.EnsureSuccessStatusCode();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
