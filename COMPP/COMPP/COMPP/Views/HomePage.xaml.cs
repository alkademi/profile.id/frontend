﻿using COMPP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        private HomeViewModel ContentViewModel { get; set; }

        public HomePage()
        {
            InitializeComponent();
            BindingContext = new HomeViewModel();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var vm = this.BindingContext as HomeViewModel;
            if (vm != null)
            {
                await vm.SetProfile();
            }
        }
    }
}