﻿using COMPP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPage : ContentPage
    {
        readonly SearchViewModel vm;

        public SearchPage(string query)
        {
            InitializeComponent();
            this.BindingContext = new SearchViewModel(query);

            vm = (SearchViewModel)this.BindingContext;

            NavigationPage.SetHasNavigationBar(this, false);
        }

        public async void OnItemClicked(object sender, ItemTappedEventArgs e)
        {
            vm.IsLoading = true;

            var index = e.ItemIndex;
            var item = vm.Result[index];
            var itemID = item.accountID;
            
            var accountID = Convert.ToInt32(await SecureStorage.GetAsync("account_id"));

            if (accountID == itemID)
            {
                await Shell.Current.GoToAsync("profile");
            } 
            else
            {
                await Application.Current.MainPage.Navigation.PushAsync(new OtherProfilePage(itemID), true);
            }
            

            vm.IsLoading = false;
        }
    }
}