﻿using COMPP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConnectionPage : ContentPage
    {
        readonly ConnectionViewModel vm;

        public ConnectionPage(int accountID)
        {
            InitializeComponent();
            this.BindingContext = new ConnectionViewModel(accountID);

            vm = (ConnectionViewModel)this.BindingContext;

            NavigationPage.SetHasNavigationBar(this, false);
        }

        public async void OnItemClicked(object sender, ItemTappedEventArgs e)
        {
            vm.IsLoading = true;

            var index = e.ItemIndex;
            var item = vm.Result[index];
            var itemID = item.Id;
            var itemConnectionID = item.ConnectionID;

            await Application.Current.MainPage.Navigation.PushAsync(new OtherProfilePage(itemID, itemConnectionID), true);

            vm.IsLoading = false;
        }
    }
}