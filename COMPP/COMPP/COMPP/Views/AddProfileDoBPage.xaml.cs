﻿using COMPP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    public partial class AddProfileDoBPage : ContentPage
    {
        public AddProfileDoBPage()
        {
            InitializeComponent();
            this.BindingContext = new AddProfileDobViewModel();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}