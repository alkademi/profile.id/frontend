﻿using COMPP.ViewModels;
using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    public partial class DeleteProfilePage : ContentPage
    {
        public DeleteProfilePage()
        {
            InitializeComponent();
            this.BindingContext = new DeleteProfileViewModel();
        }
    }
}