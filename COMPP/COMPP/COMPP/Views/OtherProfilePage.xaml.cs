﻿using COMPP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OtherProfilePage : ContentPage
    {
        public OtherProfilePage(int AccountID, int ConnectionID = 0)
        {
            InitializeComponent();
            BindingContext = new OtherProfileViewModel(AccountID, ConnectionID);
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}