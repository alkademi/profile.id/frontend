﻿using System;
using System.Collections.Generic;
using COMPP.ViewModels;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
namespace COMPP.Views
{
    public partial class EditProfileAbout : ContentPage
    {
        public EditProfileAbout()
        {
            InitializeComponent();
            this.BindingContext = new EditProfileAboutViewModel();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
