﻿using COMPP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    public partial class EditProfileEducationPage : ContentPage
    {
        public EditProfileEducationPage(int educationID)
        {
            InitializeComponent();
            this.BindingContext = new EditProfileEducationViewModel(educationID);
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}