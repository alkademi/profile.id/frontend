﻿using COMPP.Models;
using COMPP.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    public partial class EditProfileIdentityPage : ContentPage
    {

        public EditProfileIdentityPage()
        {
            InitializeComponent();
            BindingContext = new EditProfileIdentityViewModel();
        }
    }
}