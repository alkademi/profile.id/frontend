﻿using System;
using System.Collections.Generic;
using COMPP.ViewModels;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace COMPP.Views
{
    public partial class EditAddProfileExperiencePage : ContentPage
    {
        public EditAddProfileExperiencePage()
        {
            InitializeComponent();
            this.BindingContext = new EditAddProfileExperienceViewModel();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
