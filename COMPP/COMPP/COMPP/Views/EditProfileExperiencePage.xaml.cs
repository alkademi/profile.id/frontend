﻿using COMPP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    public partial class EditProfileExperiencePage : ContentPage
    {
        public EditProfileExperiencePage(int experienceID)
        {
            InitializeComponent();
            this.BindingContext = new EditProfileExperienceViewModel(experienceID);
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}