﻿using COMPP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    public partial class AddProfileExperiencePage : ContentPage
    {
        public AddProfileExperiencePage()
        {
            InitializeComponent();
            this.BindingContext = new AddProfileExperienceViewModel();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}