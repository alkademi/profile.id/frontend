﻿﻿using COMPP.Services;
using COMPP.ViewModels;
using System;
using Xamarin.Forms;

namespace COMPP.Views
{
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
            this.BindingContext = new RegisterViewModel();
        }
    }
}