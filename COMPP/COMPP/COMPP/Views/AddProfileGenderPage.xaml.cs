﻿using COMPP.ViewModels;
using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    public partial class AddProfileGenderPage : ContentPage
    {
        public AddProfileGenderPage()
        {
            InitializeComponent();
            this.BindingContext = new AddProfileGenderViewModel();
        }
    }
}