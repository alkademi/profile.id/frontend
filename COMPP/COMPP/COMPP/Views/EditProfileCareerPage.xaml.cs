﻿using COMPP.Models;
using COMPP.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMPP.Views
{
    public partial class EditProfileCareerPage : ContentPage
    {

        public EditProfileCareerPage()
        {
            InitializeComponent();
            BindingContext = new EditProfileCareerViewModel();
        }

        public async void EditExperience_Clicked(object sender, EventArgs args)
        {
            ImageButton btn = (ImageButton)sender;

            int experienceID = (int) btn.CommandParameter;

            await Navigation.PushAsync(new EditProfileExperiencePage(experienceID));

        }

        public async void EditEducation_Clicked(object sender, EventArgs args)
        {
            ImageButton btn = (ImageButton)sender;

            int educationID = (int) btn.CommandParameter;

            await Navigation.PushAsync(new EditProfileEducationPage(educationID));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var vm = this.BindingContext as EditProfileCareerViewModel;
            if (vm != null)
            {
                vm.loadData();
            }
        }

    }
}