﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models
{
    public class Education
    {
        public int EducationID { get; set; }
        public int AccountID { get; set; }
        public string SchoolName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public Education(int eid, int aid, string sn, DateTime sd, DateTime ed, string d, string i)
        {
            EducationID = eid;
            AccountID = aid;
            SchoolName = sn;
            StartDate = sd;
            EndDate = ed;
            Description = d;
            Image = i;
        }

    }
}
