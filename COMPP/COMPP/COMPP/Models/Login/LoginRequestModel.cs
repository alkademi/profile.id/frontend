﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.Login
{
    internal class LoginRequestModel
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
