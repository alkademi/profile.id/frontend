﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.Login
{
    internal class LoginResponseModel
    {
        public string Token { get; set; }
        public int ID { get; set; }
        public string Email{ get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string City { get; set; }
    }
}
