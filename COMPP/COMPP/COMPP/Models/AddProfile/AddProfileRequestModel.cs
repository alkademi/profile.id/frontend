﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.AddProfile
{
    internal class AddProfileRequestModel
    {
        public string name { get; set; }
        public int gender { get; set; }
        public string city { get; set; }
        public string picture { get; set; }
        public DateTime dateOfBirth { get; set; }
        public string description { get; set; }
        public EducationRequestModel[] educations { get; set; }
        public ExperienceRequestModel[] experiences { get; set; }
    }
}
