﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.AddProfile
{
    internal class ExperienceRequestModel
    {
        public int accountID { get; set; }
        public string experienceName { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string title { get; set; }
        public string description { get; set; }
    }
}
