﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.AddProfile
{
    internal class EducationRequestModel
    {
        public int accountID { get; set; }
        public string schoolName { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string description { get; set; }
    }
}
