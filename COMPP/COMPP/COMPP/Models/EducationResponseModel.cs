﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models
{
    internal class EducationResponseModel
    {
        public int educationID { get; set; }
        public int accountID { get; set; }
        public string schoolName { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string description { get; set; }
    }
}
