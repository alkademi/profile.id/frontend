﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.Register
{
    internal class RegisterRequestModel
    {
        public string email { get; set; }
        public string name { get; set; }
        public string password { get; set; }
    }
}
