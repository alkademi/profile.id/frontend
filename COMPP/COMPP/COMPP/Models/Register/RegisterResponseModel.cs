﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.Register
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class RegisterResponseModel
    {
        public int accountID { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public int gender { get; set; }
        public object picture { get; set; }
        public DateTime dateOfBirth { get; set; }
        public object description { get; set; }
        public object accountExperiences { get; set; }
        public object accountEducations { get; set; }
        public object fromConnections { get; set; }
        public object toConnections { get; set; }
    }

}
