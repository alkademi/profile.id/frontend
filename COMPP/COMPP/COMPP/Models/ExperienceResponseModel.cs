﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models
{
    internal class ExperienceResponseModel
    {
        public int experienceID { get; set; }
        public int accountID { get; set; }
        public string experienceName { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string title { get; set; }
        public string description { get; set; }
    }
}
