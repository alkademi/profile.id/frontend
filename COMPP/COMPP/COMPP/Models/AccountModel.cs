﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models
{
    internal class AccountModel
    {
        public int AccountID { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string City { get; set; }
        public string Picture { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Description { get; set; }
    }
}
