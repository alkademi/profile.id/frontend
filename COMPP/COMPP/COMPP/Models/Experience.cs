﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace COMPP.Models
{
    public class Experience : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public int ExperienceID { get; set; }

        public int AccountID { get; set; }
        public string ExperienceName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public string Image { get; set; }
        public Experience(int eid, int aid, string en, DateTime sd, DateTime ed, string t, string d, string i)
        {
            ExperienceID = eid;
            AccountID = aid;
            ExperienceName = en;
            StartDate = sd;
            EndDate = ed;
            Title = t;
            Description = d;
            Image = i;
        }

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

    }
}
