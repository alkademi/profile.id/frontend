﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.Connection
{
    internal class ConnectionProfile
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string City { get; set; }
        public string Picture { get; set; }
        public object pictureObj { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Description { get; set; }
        public EducationResponseModel[] Educations { get; set; }
        public ExperienceResponseModel[] Experiences { get; set; }
    }
}
