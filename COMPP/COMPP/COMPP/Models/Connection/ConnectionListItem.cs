﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.Connection
{
    internal class ConnectionListItem
    {
        public int ConnectionID { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string City { get; set; }
        public string Picture { get; set; }
        public object pictureObj { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Description { get; set; }
        public string LastEducation { get; set; }
        public string LastExperience { get; set; }
    }
}
