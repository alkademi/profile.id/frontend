﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.Connection
{
    internal class ConnectionResponseModel
    {
        public int ConnectionID { get; set; }
        public ConnectionProfile ProfileInfo { get; set; }
    }
}
