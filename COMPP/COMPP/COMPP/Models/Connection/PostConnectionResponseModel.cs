﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.Connection
{
    internal class PostConnectionResponseModel
    {
        public int ConnectionID { get; set; }
        public int FromAccountID { get; set; }
        public int ToAccountID { get; set; }
    }
}
