﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models
{
    internal class ProfileResponseModel
    {
        public string name { get; set; }
        public string email { get; set; }
        public int gender { get; set; }
        public string city { get; set; }
        public string picture { get; set; }
        public DateTime dateOfBirth { get; set; }
        public string description { get; set; }
        public List<EducationResponseModel> educations { get; set; }
        public List<ExperienceResponseModel> experiences { get; set; }
    }
}
