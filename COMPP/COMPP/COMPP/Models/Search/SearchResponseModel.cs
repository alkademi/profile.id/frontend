﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMPP.Models.Search
{
    public class SearchResponseModel
    {
        public int accountID { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public int gender { get; set; }
        public string city { get; set; }
        public string picture { get; set; }
        public object pictureObj { get; set; }
        public DateTime dateOfBirth { get; set; }
        public object description { get; set; }
    }

}
