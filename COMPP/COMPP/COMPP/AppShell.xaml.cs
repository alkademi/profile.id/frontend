using COMPP.ViewModels;
using COMPP.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace COMPP
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute("login", typeof(LoginPage));
            Routing.RegisterRoute("register", typeof(RegisterPage));
            Routing.RegisterRoute("dob", typeof(AddProfileDoBPage));
            Routing.RegisterRoute("education", typeof(AddProfileEducationPage));
            Routing.RegisterRoute("experience", typeof(AddProfileExperiencePage));
            Routing.RegisterRoute("picture", typeof(AddProfilePicturePage));
            //Routing.RegisterRoute("home", typeof(HomePage));
            Routing.RegisterRoute("profile", typeof(ProfilePage));
            Routing.RegisterRoute("edit-about", typeof(EditProfileAbout));
            Routing.RegisterRoute("edit-addexperience", typeof(EditAddProfileExperiencePage));
            Routing.RegisterRoute("edit-addeducation", typeof(EditAddProfileEducationPage));
            Routing.RegisterRoute("edit-identity", typeof(EditProfileIdentityPage));
            Routing.RegisterRoute("edit-career", typeof(EditProfileCareerPage));
            Routing.RegisterRoute("edit-experience", typeof(EditProfileExperiencePage));
            Routing.RegisterRoute("edit-education", typeof(EditProfileEducationPage));
            Routing.RegisterRoute("add-experience", typeof(EditAddProfileExperiencePage));
            Routing.RegisterRoute("add-education", typeof(EditAddProfileEducationPage));
            Routing.RegisterRoute("delete-profile", typeof(DeleteProfilePage));
            BindingContext = this;
        }

    }
}
