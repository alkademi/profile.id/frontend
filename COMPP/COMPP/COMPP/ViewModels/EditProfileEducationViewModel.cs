﻿using COMPP.Services;
using COMPP.Models.AddProfile;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Windows.Input;
using System.Threading.Tasks;
using Xamarin.Essentials;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace COMPP.ViewModels
{
    public class EditProfileEducationViewModel : INotifyPropertyChanged
    {
        public ICommand EditEducation { get; }
        public ICommand DeleteEducation { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        public EditProfileEducationViewModel(int educationID)
        {
            GetEducations(educationID);
            EditEducation = new Command(async () => await OnSaveClicked(educationID));
            DeleteEducation = new Command(async () => await OnDeleteClicked(educationID));
        }
        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        string _schoolName;
        public string SchoolName
        {
            get { return _schoolName; }
            set
            {
                if (_schoolName != value)
                {
                    _schoolName = value;
                    OnPropertyChanged(nameof(SchoolName));
                }
            }
        }

        string _jurusan;
        public string Jurusan
        {
            get { return _jurusan; }
            set
            {
                if (_jurusan != value)
                {
                    _jurusan = value;
                    OnPropertyChanged(nameof(Jurusan));
                }
            }
        }

        DateTime _eduStartDate;
        public DateTime EduStartDate
        {
            get { return _eduStartDate; }
            set
            {
                if (_eduStartDate != value)
                {
                    _eduStartDate = value;
                    OnPropertyChanged(nameof(EduStartDate));
                }
            }
        }

        DateTime _eduEndDate;

        public DateTime EduEndDate
        {
            get { return _eduEndDate; }
            set
            {
                if (_eduEndDate != value)
                {
                    _eduEndDate = value;
                    OnPropertyChanged(nameof(EduEndDate));
                }
            }
        }

        private async void GetEducations(int educationId)
        {
            var response = await AccountService.ServiceClientInstance.GetEducations(educationId);

            if (response == null)
            {
            }
            else
            {
                SchoolName = response.schoolName;
                EduStartDate = response.startDate;
                EduEndDate = response.endDate;
                Jurusan = response.description;
            }

        }

        private async Task OnSaveClicked(int educationID)
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));; 
            EducationRequestModel eduInfo = new EducationRequestModel()
            {
                accountID = id,
                schoolName = SchoolName,
                startDate = EduStartDate,
                endDate = EduEndDate,
                description = Jurusan
            };
            await AccountService.ServiceClientInstance.EditEducation(educationID, eduInfo);
            await Shell.Current.GoToAsync("..");
        }

        private async Task OnDeleteClicked(int educationID)
        {
            await AccountService.ServiceClientInstance.DeleteEducation(educationID);
            await Shell.Current.GoToAsync("..");
        }
    }
}
