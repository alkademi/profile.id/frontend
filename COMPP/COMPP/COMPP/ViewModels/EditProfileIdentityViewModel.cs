﻿using System;
using Xamarin.Forms;
using System.Windows.Input;
using System.Threading.Tasks;
using System.IO;
using Xamarin.Essentials;
using COMPP.Services;
using System.ComponentModel;
using COMPP.Models.EditProfile;
using System.Runtime.CompilerServices;

namespace COMPP.ViewModels
{
    public class EditProfileIdentityViewModel : INotifyPropertyChanged
    {
        // dummy
        public event PropertyChangedEventHandler PropertyChanged;

        private string base64Image = "";
        public Command PictureCommand { get; }

        public ICommand EditIdentity { get; }

        string _name;
        string _description;
        string _location;
        DateTime _date;

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged(nameof(Name));
                }
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (_description != value)
                {
                    _description = value;
                    OnPropertyChanged(nameof(Description));
                }
            }
        }

        public string Location
        {
            get { return _location; }
            set
            {
                if (_location != value)
                {
                    _location = value;
                    OnPropertyChanged(nameof(Location));
                }
            }
        }

        public DateTime Date
        {
            get { return _date; }
            set
            {
                if (_date != value)
                {
                    _date = value;
                    OnPropertyChanged(nameof(Date));
                }
            }
        }

        ImageSource _imageSource = "dummy_profile4x.png";
        public ImageSource ImageSource
        {
            get { return _imageSource; }
            set
            {
                if (_imageSource != value)
                {
                    _imageSource = value;
                    OnPropertyChanged(nameof(ImageSource));
                }
            }
        }

        private async void GetIdentity()
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            var response = await AccountService.ServiceClientInstance.GetProfile(id);

            if (response == null)
            {
            }
            else
            {
                Name = response.name;
                Description = response.description;
                Location = response.city;
                Date = response.dateOfBirth;
                base64Image = response.picture;

                if (!string.IsNullOrWhiteSpace(base64Image))
                {
                    try
                    {
                        byte[] Base64Stream = Convert.FromBase64String(base64Image);
                        ImageSource = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                    }
                    catch (Exception ex)
                    {
                        ImageSource = ImageSource.FromFile("dummy_profile4x.png");
                    }
                }
                else
                {
                    ImageSource = ImageSource.FromFile("dummy_profile4x.png");
                }
            }
        }


        private async Task OnPictureSelectClicked()
        {
            var result = await MediaPicker.PickPhotoAsync(new MediaPickerOptions
            {
                Title = "Select a profile picture"
            });

            if (result != null)
            {
                var stream = await result.OpenReadAsync();

                var bytes = new byte[stream.Length];
                await stream.ReadAsync(bytes, 0, (int)stream.Length);
                base64Image = Convert.ToBase64String(bytes);

                ImageSource = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(base64Image)));

            }
        }


        public EditProfileIdentityViewModel() {
            GetIdentity();
            PictureCommand = new Command(async () => await OnPictureSelectClicked());
            EditIdentity = new Command(async () => await OnSaveClicked());
        }

        private async Task OnSaveClicked()
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            EditProfileRequestModel profileInfo = new EditProfileRequestModel()
            {
                gender = int.Parse(await SecureStorage.GetAsync("gender")),
                email = await SecureStorage.GetAsync("email"),
                name = Name,
                city = Location,
                picture = base64Image,
                dateOfBirth = Date,
                description = Description
            };

            await AccountService.ServiceClientInstance.EditProfileIdentity(id, profileInfo);
            await Shell.Current.GoToAsync("..");
        }
    }
}
