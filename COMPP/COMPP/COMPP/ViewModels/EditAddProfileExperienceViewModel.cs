﻿using System;

using Xamarin.Forms;
using COMPP.Services;
using COMPP.Models;
using COMPP.Models.AddProfile;
using System.Collections.Generic;
using System.Windows.Input;
using System.Threading.Tasks;
using Xamarin.Essentials;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace COMPP.ViewModels
{
    public class EditAddProfileExperienceViewModel : INotifyPropertyChanged
    {
        public ICommand EditExperience { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        public EditAddProfileExperienceViewModel()
        {
            EditExperience = new Command(async () => await OnSaveClicked());

        }

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }


        string _companyName = "";
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                if (_companyName != value)
                {
                    _companyName = value;
                    OnPropertyChanged(nameof(CompanyName));
                }
            }
        }

        string _position = "";
        public string Position
        {
            get { return _position; }
            set
            {
                if (_position != value)
                {
                    _position = value;
                    OnPropertyChanged(nameof(Position));
                }
            }
        }

        DateTime _expStartDate = DateTime.Today;
        public DateTime ExpStartDate
        {
            get { return _expStartDate; }
            set
            {
                if (_expStartDate != value)
                {
                    _expStartDate = value;
                    OnPropertyChanged(nameof(ExpStartDate));
                }
            }
        }

        DateTime _expEndDate = DateTime.Today;

        public DateTime ExpEndDate
        {
            get { return _expEndDate; }
            set
            {
                if (_expEndDate != value)
                {
                    _expEndDate = value;
                    OnPropertyChanged(nameof(ExpEndDate));
                }
            }
        }

        private async Task OnSaveClicked()
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            ExperienceRequestModel expInfo = new ExperienceRequestModel()
            {
                accountID = id,
                experienceName = CompanyName,
                startDate = ExpStartDate,
                endDate = ExpEndDate,
                title = Position,

            };
            await AccountService.ServiceClientInstance.AddExperience(expInfo);
            await Shell.Current.GoToAsync("..");
        }

    }
}

