﻿using COMPP.Models.Search;
using COMPP.Services;
using COMPP.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace COMPP.ViewModels
{
    internal class SearchViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand SearchCommand { get; private set; }
        public ICommand BtnNameClick { get; private set; }
        public ICommand BtnJobClick { get; private set; }
        public ICommand BtnCompanyClick { get; private set; }
        public ICommand ItemClick { get; private set; }

        string btnNameColor = "White";
        public string BtnNameColor
        {
            get { return btnNameColor; }
            set
            {
                if(btnNameColor != value)
                {
                    btnNameColor = value;
                    OnPropertyChanged(nameof(BtnNameColor));
                }
            }
        }

        string btnJobColor = "#47B2BA";
        public string BtnJobColor
        {
            get { return btnJobColor; }
            set
            {
                if (btnJobColor != value)
                {
                    btnJobColor = value;
                    OnPropertyChanged(nameof(BtnJobColor));
                }
            }
        }

        string btnCompanyColor = "#47B2BA";
        public string BtnCompanyColor
        {
            get { return btnCompanyColor; }
            set
            {
                if (btnCompanyColor != value)
                {
                    btnCompanyColor = value;
                    OnPropertyChanged(nameof(BtnCompanyColor));
                }
            }
        }

        string btnNameBgColor = "#47B2BA";
        public string BtnNameBgColor
        {
            get { return btnNameBgColor; }
            set
            {
                if (btnNameBgColor != value)
                {
                    btnNameBgColor = value;
                    OnPropertyChanged(nameof(BtnNameBgColor));
                }
            }
        }

        string btnJobBgColor = "White";
        public string BtnJobBgColor
        {
            get { return btnJobBgColor; }
            set
            {
                if (btnJobBgColor != value)
                {
                    btnJobBgColor = value;
                    OnPropertyChanged(nameof(BtnJobBgColor));
                }
            }
        }

        string btnCompanyBgColor = "White";
        public string BtnCompanyBgColor
        {
            get { return btnCompanyBgColor; }
            set
            {
                if (btnCompanyBgColor != value)
                {
                    btnCompanyBgColor = value;
                    OnPropertyChanged(nameof(BtnCompanyBgColor));
                }
            }
        }

        private string _searchQuery;
        public string SearchQuery
        {
            get { return _searchQuery; }
            set
            {
                if (_searchQuery != value)
                {
                    _searchQuery = value;
                    OnPropertyChanged(nameof(_searchQuery));
                }
            }
        }

        private string _category = "name";
        public string Category
        {
            get => _category;
            set
            {
                if (_category != value)
                {
                    _category = value;
                    OnPropertyChanged(nameof(Category));
                }
            }
        }

        bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                if (_isLoading != value)
                {
                    _isLoading = value;
                    OnPropertyChanged(nameof(IsLoading));
                }
            }
        }

        private ObservableCollection<SearchResponseModel> _result;
        public ObservableCollection<SearchResponseModel> Result
        {
            get => _result;
            set
            {
                if (value != _result)
                {
                    _result = value;
                    OnPropertyChanged(nameof(Result));
                }
            }
        }
        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public SearchViewModel(string query)
        {
            Result = GetSearchResult(query);
            SearchQuery = query;
            SearchCommand = new Command<string>((string searchQuery) => OnSearchClicked(searchQuery));
            BtnNameClick = new Command(() => OnBtnNameClicked());
            BtnJobClick = new Command(() => OnBtnJobClicked());
            BtnCompanyClick = new Command(() => OnBtnCompanyClicked());
        }

        private ObservableCollection<SearchResponseModel> GetSearchResult(string query, string category = "name")
        {
            IsLoading = true;
            var result = new List<SearchResponseModel>();
            var responseResult = new List<SearchResponseModel>();
            if (Category == "name")
            {
                responseResult = Task.Run(() =>  SearchService.ServiceClientInstance.SearchByName(query)).Result;
            }
            else if (Category == "job")
            {
                responseResult = Task.Run(() => SearchService.ServiceClientInstance.SearchByJob(query)).Result;
            }
            else if (Category == "company")
            {
                responseResult = Task.Run(() => SearchService.ServiceClientInstance.SearchByCompany(query)).Result;
            }
            else
            {
                responseResult = null;
            }

            responseResult.ForEach(async item =>
            {
                var ID = await SecureStorage.GetAsync("account_id");
                if (item.accountID != int.Parse(ID))
                {
                    if (item.picture != null && item.picture != "")
                    {
                        try
                        {
                            byte[] Base64Stream = Convert.FromBase64String(item.picture);
                            item.pictureObj = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                        }
                        catch (Exception ex)
                        {
                            item.pictureObj = ImageSource.FromFile("dummy_profile4x.png");
                        }
                    } else
                    {
                        item.pictureObj = ImageSource.FromFile("dummy_profile4x.png");
                    }

                    result.Add(item);
                }
            });

            var convertedResult = new ObservableCollection<SearchResponseModel>(result);

            IsLoading = false;

            return convertedResult;
        }
        private void OnSearchClicked(string query)
        {
            SearchQuery = query;
            Result = GetSearchResult(query, Category);
        }

        private void OnBtnNameClicked()
        {
            Category = "name";
            Result = GetSearchResult(SearchQuery);
            BtnNameColor = "White";
            BtnNameBgColor =  "#47B2BA";
            BtnJobColor = "#47B2BA"; 
            BtnJobBgColor = "White";
            BtnCompanyColor = "#47B2BA";
            BtnCompanyBgColor = "White";
        }

        private void OnBtnJobClicked()
        {
            Category = "job";
            Result = GetSearchResult(SearchQuery);
            BtnJobColor = "White";
            BtnJobBgColor = "#47B2BA";
            BtnNameColor = "#47B2BA";
            BtnNameBgColor = "White";
            BtnCompanyColor = "#47B2BA";
            BtnCompanyBgColor = "White";
        }

        private void OnBtnCompanyClicked()
        {
            Category = "company";
            Result = GetSearchResult(SearchQuery);
            BtnCompanyColor = "White";
            BtnCompanyBgColor = "#47B2BA";
            BtnJobColor = "#47B2BA";
            BtnJobBgColor = "White";
            BtnNameColor = "#47B2BA";
            BtnNameBgColor = "White";
        }
    }
}
