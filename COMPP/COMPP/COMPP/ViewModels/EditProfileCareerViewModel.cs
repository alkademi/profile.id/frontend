﻿using COMPP.Models;
using COMPP.Services;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Linq;
using Xamarin.Essentials;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace COMPP.ViewModels
{
    public class EditProfileCareerViewModel : INotifyPropertyChanged
    {
        public ICommand LoadMoreExperience { get; private set; }
        public ICommand LoadMoreEducation { get; private set; }
        public ICommand EditExperience { get; private set; }
        public ICommand EditEducation { get; private set; }
        public ICommand AddExperience { get; private set; }
        public ICommand AddEducation { get; private set; }
        public ICommand Back { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        // init
        public ObservableCollection<Experience> ExperienceList { get; set; } = new ObservableCollection<Experience>();

        public ObservableCollection<Experience> ExperienceListShow { get; set; } = new ObservableCollection<Experience>();
        public ObservableCollection<Education> EducationList { get; set; } = new ObservableCollection<Education>();
        public ObservableCollection<Education> EducationListShow { get; set; } = new ObservableCollection<Education>();

        private async void GetEducationList()
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            var response = await AccountService.ServiceClientInstance.GetProfile(id);

            if (response == null)
            {
                
            }
            else
            {
                EducationList.Clear();
                for (var i = 0; i < response.educations.Count; i++)
                {
                    EducationList.Add(new Education(response.educations[i].educationID, response.educations[i].accountID, response.educations[i].schoolName, response.educations[i].startDate, response.educations[i].endDate, response.educations[i].description, "icon_school.png"));
                }
                if (EducationList.Count > 0)
                {
                    EducationListShow = new ObservableCollection<Education> {
                EducationList.ElementAt(0),
                };
                    EducationListShow[0] = EducationList.ElementAt(0);
                }
                else
                {
                    EducationListShow.Clear();
                }
                if (EducationList.Count > 1)
                {
                    LoadMoreStateEducation = true;
                }
                else
                {
                    LoadMoreStateEducation = false;
                }
                OnPropertyChanged("EducationList");
                OnPropertyChanged("EducationHeight");
                OnPropertyChanged("EducationListShow");
                OnPropertyChanged("LoadMoreStateEducation");
            }
        }

        private async void GetExperienceList()
        {
    
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            var response = await AccountService.ServiceClientInstance.GetProfile(id);

            if (response == null)
            {

            }
            else
            {
                ExperienceList.Clear();
                for (var i = 0; i < response.experiences.Count; i++)
                {
                    ExperienceList.Add(new Experience(response.experiences[i].experienceID, response.experiences[i].accountID, response.experiences[i].experienceName, response.experiences[i].startDate, response.experiences[i].endDate, response.experiences[i].title, response.experiences[i].description, "icon_company.png"));
                }
                int max = 3;
                if (ExperienceList.Count < max)
                {
                    max = ExperienceList.Count;
                }
                if (ExperienceList.Count > 0)
                {
                    ExperienceListShow = new ObservableCollection<Experience> {
                    ExperienceList.ElementAt(0),
                 };
                    for (var i = 1; i < max; i++)
                    {
                        ExperienceListShow.Add(ExperienceList.ElementAt(i));
                    }
                }
                else
                {
                    ExperienceListShow.Clear();
                }
                if (ExperienceList.Count > 3)
                {
                    LoadMoreStateExperience = true;
                }
                else
                {
                    LoadMoreStateExperience = false;
                }
                OnPropertyChanged("ExperienceList");
                OnPropertyChanged("ExperienceHeight");
                OnPropertyChanged("ExperienceListShow");
                OnPropertyChanged("LoadMoreStateExperience");
            }
        }

        public int ExperienceHeight
        {
            get => (ExperienceListShow.Count) * 72;
        }

        public int EducationHeight
        {
            get => (EducationListShow.Count) * 72;
        }

        public bool LoadMoreStateExperience { get; set; }

        public bool LoadMoreStateEducation { get; set; }

        public EditProfileCareerViewModel()
        {
            LoadMoreExperience = new Command(async () => await OnLoadMoreExperience());
            LoadMoreEducation = new Command(async () => await OnLoadMoreEducation());
            Back = new Command(async () => await Shell.Current.GoToAsync(".."));
            EditEducation = new Command(async () => await Shell.Current.GoToAsync("edit-education"));
            EditExperience = new Command(async () => await Shell.Current.GoToAsync("edit-experience"));
            AddEducation = new Command(async () => await Shell.Current.GoToAsync("add-education"));
            AddExperience = new Command(async () => await Shell.Current.GoToAsync("add-experience"));
            loadData();
        }

        public void loadData()
        {
            GetExperienceList();
            GetEducationList();
        }

        public async Task OnLoadMoreExperience()
        {
            await Task.Run(() =>
            ExperienceListShow = ExperienceList);
            LoadMoreStateExperience = false;
            OnPropertyChanged("LoadMoreStateExperience");
            OnPropertyChanged("ExperienceListShow");
            OnPropertyChanged("ExperienceHeight");
        }

        public async Task OnLoadMoreEducation()
        {
            await Task.Run(() =>
            EducationListShow = EducationList);
            LoadMoreStateEducation = false;
            OnPropertyChanged("LoadMoreStateEducation");
            OnPropertyChanged("EducationListShow");
            OnPropertyChanged("EducationHeight");
        }

    }
}
