﻿using COMPP.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace COMPP.ViewModels
{
    public class AddProfileExperienceViewModel : INotifyPropertyChanged
    {
        public ICommand Picture { get; }

        public event PropertyChangedEventHandler PropertyChanged;
        public AddProfileExperienceViewModel()
        {
            Picture = new Command(async () => await OnNextClicked());
        }

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        string _companyName = "";
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                if (_companyName != value)
                {
                    _companyName = value;
                    OnPropertyChanged(nameof(CompanyName));
                }
            }
        }

        string _position = "";
        public string Position
        {
            get { return _position; }
            set
            {
                if (_position != value)
                {
                    _position = value;
                    OnPropertyChanged(nameof(Position));
                }
            }
        }

        DateTime _expStartDate = DateTime.Today;
        public DateTime ExpStartDate
        {
            get { return _expStartDate; }
            set
            {
                if (_expStartDate != value)
                {
                    _expStartDate = value;
                    OnPropertyChanged(nameof(ExpStartDate));
                }
            }
        }

        DateTime _expEndDate = DateTime.Today;

        public DateTime ExpEndDate
        {
            get { return _expEndDate; }
            set
            {
                if (_expEndDate != value)
                {
                    _expEndDate = value;
                    OnPropertyChanged(nameof(ExpEndDate));
                }
            }
        }
        private async Task OnNextClicked()
        {
            await SecureStorage.SetAsync("company_name", CompanyName);
            await SecureStorage.SetAsync("position", Position);
            await SecureStorage.SetAsync("exp_start_date", ExpStartDate.ToString());
            await SecureStorage.SetAsync("exp_end_date", ExpEndDate.ToString());
            await Shell.Current.GoToAsync("picture");
        }
    }
}
