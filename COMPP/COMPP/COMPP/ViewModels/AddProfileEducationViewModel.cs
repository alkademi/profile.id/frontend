﻿using COMPP.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using System.Threading.Tasks;
using Xamarin.Essentials;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace COMPP.ViewModels
{
    public class AddProfileEducationViewModel : INotifyPropertyChanged
    {
        public ICommand Experience { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        public AddProfileEducationViewModel()
        {
            Experience = new Command(async () => await OnNextClicked());
        }
        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        string _schoolName = "";
        public string SchoolName
        {
            get { return _schoolName; }
            set
            {
                if (_schoolName != value)
                {
                    _schoolName = value;
                    OnPropertyChanged(nameof(SchoolName));
                }
            }
        }

        string _jurusan = "";
        public string Jurusan
        {
            get { return _jurusan; }
            set
            {
                if (_jurusan != value)
                {
                    _jurusan = value;
                    OnPropertyChanged(nameof(Jurusan));
                }
            }
        }

        DateTime _eduStartDate = DateTime.Today;
        public DateTime EduStartDate
        {
            get { return _eduStartDate; }
            set
            {
                if (_eduStartDate != value)
                {
                    _eduStartDate = value;
                    OnPropertyChanged(nameof(EduStartDate));
                }
            }
        }

        DateTime _eduEndDate = DateTime.Today;

        public DateTime EduEndDate
        {
            get { return _eduEndDate; }
            set
            {
                if (_eduEndDate != value)
                {
                    _eduEndDate = value;
                    OnPropertyChanged(nameof(EduEndDate));
                }
            }
        }

        private async Task OnNextClicked()
        {
            await SecureStorage.SetAsync("school_name", SchoolName);
            await SecureStorage.SetAsync("jurusan", Jurusan);
            await SecureStorage.SetAsync("edu_start_date", EduStartDate.ToString());
            await SecureStorage.SetAsync("edu_end_date", EduEndDate.ToString());
            await Shell.Current.GoToAsync("experience");
        }
    }
}
