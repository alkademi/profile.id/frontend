﻿using COMPP.Models;
using COMPP.Services;
using COMPP.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.CommunityToolkit.Extensions;
using System.IO;

namespace COMPP.ViewModels
{
    public class OtherProfileViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand UpdateFollowStatusCommand { get; private set; }
        public ICommand ConnectionCommand { get; private set; }
        public ICommand LoadMoreExperience { get; private set; }
        public ICommand LoadMoreEducation { get; private set; }
        public ICommand PageAppearingCommand { get; private set; }
        public ICommand Back { get; private set; }

        private string base64Image = "";
        string _name;
        string _description;
        string _about;
        string _location;

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }


        //init
        public ObservableCollection<Experience> ExperienceList { get; set; } = new ObservableCollection<Experience>();

        public ObservableCollection<Experience> ExperienceListShow { get; set; } = new ObservableCollection<Experience>();
        public ObservableCollection<Education> EducationList { get; set; } = new ObservableCollection<Education>();
        public ObservableCollection<Education> EducationListShow { get; set; } = new ObservableCollection<Education>();

        private int _otherAccountID;

        private int _connectionID = 0;
        public int ConnectionID { 
            get { return _connectionID; } 
            set {
                if (_connectionID != value)
                {
                    _connectionID = value;
                    OnPropertyChanged(nameof(ConnectionID));
                }
            } 
        }

        private bool _isConnected = false;
        public bool IsConnected
        {
            get => _isConnected;
            set {
                if (_isConnected != value)
                {
                    _isConnected = value;
                    OnPropertyChanged(nameof(IsConnected));
                }
            }
        }

        bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                if (_isLoading != value)
                {
                    _isLoading = value;
                    OnPropertyChanged(nameof(IsLoading));
                }
            }
        }

        private string _iconFollowColor;
        public string IconFollowColor
        {
            get => _iconFollowColor;
            set
            {
                if (_iconFollowColor != value)
                {
                    _iconFollowColor = value;
                    OnPropertyChanged(nameof(IconFollowColor));
                }
            }
        }

        private ImageSource _followImage;
        public ImageSource FollowImage
        {
            get => _followImage;
            set
            {
                if (_followImage != value)
                {
                    _followImage = value;
                    OnPropertyChanged(nameof(FollowImage));
                }
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged(nameof(Name));
                }
            }
        }

        string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                if (_email != value)
                {
                    _email = value;
                    OnPropertyChanged(nameof(Email));
                }
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (_description != value)
                {
                    _description = value;
                    OnPropertyChanged(nameof(Description));
                }
            }
        }

        public string Location
        {
            get { return _location; }
            set
            {
                if (_location != value)
                {
                    _location = value;
                    OnPropertyChanged(nameof(Location));
                }
            }
        }

        public string About
        {
            get { return _about; }
            set
            {
                if (_about != value)
                {
                    _about = value;
                    OnPropertyChanged(nameof(About));
                }
            }
        }

        ImageSource _imageSource = "dummy_profile4x.png";
        public ImageSource Picture
        {
            get { return _imageSource; }
            set
            {
                if (_imageSource != value)
                {
                    _imageSource = value;
                    OnPropertyChanged(nameof(Picture));
                }
            }
        }


        public int ExperienceHeight
        {
            get => (ExperienceListShow.Count) * 72;
        }

        public int EducationHeight
        {
            get => (EducationListShow.Count) * 72;
        }

        public bool LoadMoreStateExperience { get; set; }

        public bool LoadMoreStateEducation { get; set; }

        public int ConnectionCount { get; set; }


        public OtherProfileViewModel(int accountID, int connectionID)
        {
            _otherAccountID = accountID;

            if (connectionID != 0)
            {
                ConnectionID = connectionID;
                IsConnected = true;
            }

            // init command
            PageAppearingCommand = new Command(async () =>
            {
                IsLoading = true;
                await GetIdentity();
                await GetExperienceList();
                await GetEducationList();
                await SetupConnectionUI();
                await GetConnectionList(_otherAccountID);
                IsLoading = false;
            });
            LoadMoreExperience = new Command(async () => await OnLoadMoreExperience());
            LoadMoreEducation = new Command(async () => await OnLoadMoreEducation());
            Back = new Command(async () => await Shell.Current.GoToAsync(".."));
            UpdateFollowStatusCommand = new Command(async () => await OnFollowButtonClickedAsync());
            ConnectionCommand = new Command(async () => await OnConnectionInfoClicked());
        }

        private async Task OnFollowButtonClickedAsync()
        {
            var isError = await UpdateConnectionStatus();
            if (!isError)
            {
                UpdateConnectionUI();
                if (IsConnected)
                {
                    await App.Current.MainPage.DisplayToastAsync("Followed!");
                }
                else
                {
                    await App.Current.MainPage.DisplayToastAsync("Unfollowed!");
                }
            }
        }

        public async Task OnLoadMoreExperience()
        {
            await Task.Run(() =>
            ExperienceListShow = ExperienceList);
            LoadMoreStateExperience = false;
            OnPropertyChanged("LoadMoreStateExperience");
            OnPropertyChanged("ExperienceListShow");
            OnPropertyChanged("ExperienceHeight");
        }

        public async Task OnLoadMoreEducation()
        {
            await Task.Run(() =>
            EducationListShow = EducationList);
            LoadMoreStateEducation = false;
            OnPropertyChanged("LoadMoreStateEducation");
            OnPropertyChanged("EducationListShow");
            OnPropertyChanged("EducationHeight");
        }

        private async Task GetIdentity()
        {
            var response = await AccountService.ServiceClientInstance.GetProfile(_otherAccountID);

            if (response == null)
            {
            }
            else
            {
                Name = response.name;
                Email = response.email;
                About = response.description;
                Location = response.city;
                base64Image = response.picture;

                if (!string.IsNullOrWhiteSpace(base64Image))
                {
                    try
                    {
                        byte[] Base64Stream = Convert.FromBase64String(base64Image);
                        Picture = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                    }
                    catch (Exception ex)
                    {
                        Picture = ImageSource.FromFile("dummy_profile4x.png");
                    }
                }
                else
                {
                    Picture = ImageSource.FromFile("dummy_profile4x.png");
                }

                if (response.educations.Count > 0)
                {
                    Description = response.educations[response.educations.Count - 1].description + " - " + response.educations[response.educations.Count - 1].schoolName;
                    if (response.experiences.Count > 0)
                    {
                        Description = Description + " | " + response.experiences[response.experiences.Count - 1].title + " - " + response.experiences[response.experiences.Count - 1].experienceName;
                    }
                }
                else
                {
                    if (response.experiences.Count > 0)
                    {
                        Description = Description + response.experiences[response.experiences.Count - 1].title + " - " + response.experiences[response.experiences.Count - 1].experienceName;
                    }
                }
            }
        }

        private async Task GetEducationList()
        {
            var response = await AccountService.ServiceClientInstance.GetProfile(_otherAccountID);

            if (response == null)
            {

            }
            else
            {
                EducationList.Clear();
                for (var i = 0; i < response.educations.Count; i++)
                {
                    EducationList.Add(new Education(response.educations[i].educationID, response.educations[i].accountID, response.educations[i].schoolName, response.educations[i].startDate, response.educations[i].endDate, response.educations[i].description, "icon_school.png"));
                }
                if (EducationList.Count > 0)
                {
                    EducationListShow = new ObservableCollection<Education> {
                    EducationList.ElementAt(0),
                };
                    EducationListShow[0] = EducationList.ElementAt(0);
                }
                else
                {
                    EducationListShow.Clear();
                }
                if (EducationList.Count > 1)
                {
                    LoadMoreStateEducation = true;
                }
                else
                {
                    LoadMoreStateEducation = false;
                }
                OnPropertyChanged("EducationList");
                OnPropertyChanged("EducationHeight");
                OnPropertyChanged("EducationListShow");
                OnPropertyChanged("LoadMoreStateEducation");
            }
        }
        private async Task GetConnectionList(int accountID)
        {
            IsLoading = true;
            var result = await ConnectionService.ServiceClientInstance.GetConnectionList(accountID);
            if (result == null)
            {
                ConnectionCount = 0;
            }
            else
            {
                ConnectionCount = result.Count;
            }
            OnPropertyChanged("ConnectionCount");
        }
        private async Task GetExperienceList()
        {
            var response = await AccountService.ServiceClientInstance.GetProfile(_otherAccountID);

            if (response == null)
            {

            }
            else
            {
                ExperienceList.Clear();
                for (var i = 0; i < response.experiences.Count; i++)
                {
                    ExperienceList.Add(new Experience(response.experiences[i].experienceID, response.experiences[i].accountID, response.experiences[i].experienceName, response.experiences[i].startDate, response.experiences[i].endDate, response.experiences[i].title, response.experiences[i].description, "icon_company.png"));
                }
                int max = 3;
                if (ExperienceList.Count < max)
                {
                    max = ExperienceList.Count;
                }
                if (ExperienceList.Count > 0)
                {
                    ExperienceListShow = new ObservableCollection<Experience> {
                    ExperienceList.ElementAt(0),
                 };
                    for (var i = 1; i < max; i++)
                    {
                        ExperienceListShow.Add(ExperienceList.ElementAt(i));
                    }
                }
                else
                {
                    ExperienceListShow.Clear();
                }
                if (ExperienceList.Count > 3)
                {
                    LoadMoreStateExperience = true;
                }
                else
                {
                    LoadMoreStateExperience = false;
                }
                OnPropertyChanged("ExperienceList");
                OnPropertyChanged("ExperienceHeight");
                OnPropertyChanged("ExperienceListShow");
                OnPropertyChanged("LoadMoreStateExperience");
            }
        }

        public async Task<bool> GetConnectionStatus()
        {
            var userID = int.Parse(await SecureStorage.GetAsync("account_id"));
            var status = await ConnectionService.ServiceClientInstance.CheckConnectionFromTo(userID, _otherAccountID);
            

            if (status != null)
            {
                ConnectionID = status.ConnectionID;
                IsConnected = true;

            } else
            {
                IsConnected = false;
            }

            return IsConnected;
        }

        public async Task<bool> UpdateConnectionStatus()
        {
            var isError = true;
            if (IsConnected)
            {
                if (ConnectionID != 0)
                {
                    var response = await ConnectionService.ServiceClientInstance.DeleteConnection(ConnectionID);

                    if (response != false)
                    {
                        ConnectionID = 0;
                        isError = false;
                        IsConnected = !IsConnected;
                    }
                }
            } else
            {
                var userID = int.Parse(await SecureStorage.GetAsync("account_id"));
                var response = await ConnectionService.ServiceClientInstance.AddConnection(userID, _otherAccountID);

                if (response != null)
                {
                    ConnectionID = response.ConnectionID;
                    isError = false;
                    IsConnected = !IsConnected;
                }
            }

            return isError;
        }

        public async Task SetupConnectionUI()
        {
            var status = await GetConnectionStatus();
            UpdateConnectionUI();
        }

        public void UpdateConnectionUI()
        {
            if (IsConnected)
            {
                FollowImage = ImageSource.FromFile("icon_check.png");
                IconFollowColor = "#47B2BA";
            }
            else
            {
                FollowImage = ImageSource.FromFile("icon_follow.png");
                IconFollowColor = "White";
            }
        }

        public async Task OnConnectionInfoClicked()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new ConnectionPage(_otherAccountID), true);
        }
    }
}
