﻿using COMPP.Services;
using COMPP.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace COMPP.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {    
        public Command LoginCommand { get; private set; }
        public Command Signup { get; private set; }

        public LoginViewModel()
        {
            LoginCommand = new Command(async () => await OnLoginClicked());
            Signup = new Command(async () => await OnSignupClicked());
        }
    

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                if (_isLoading != value)
                {
                    _isLoading = value;
                    OnPropertyChanged(nameof(IsLoading));
                }
            }
        }

        string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                if (_email != value)
                {
                    _email = value;
                    OnPropertyChanged(nameof(Email));
                }
            }
        }

        string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                if (_password != value)
                {
                    _password = value;
                    OnPropertyChanged(nameof(Password));
                }
            }
        }

        private async Task OnLoginClicked()
        {
            IsLoading = true;
            var response = await AccountService.ServiceClientInstance.Login(Email, Password);

            if (response == null)
            {
                IsLoading = false;
                await App.Current.MainPage.DisplayAlert("Error", "Wrong email or password", "OK");
            }
            else
            {
                await SecureStorage.SetAsync("name", response.Name);
                await SecureStorage.SetAsync("account_id", response.ID.ToString());
                await SecureStorage.SetAsync("email", response.Email);
                await SecureStorage.SetAsync("gender", response.Gender.ToString());
                await SecureStorage.SetAsync("city", response.City != null ? response.City : "default");

                await Shell.Current.GoToAsync("//home");
                Password = "";
                Email = "";
                IsLoading = false;
            }
        }

        private async Task OnSignupClicked()
        {
            await Shell.Current.GoToAsync("//login/register");
        }
    }
}
