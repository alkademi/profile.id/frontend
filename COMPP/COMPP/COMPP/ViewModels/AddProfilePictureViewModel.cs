﻿using COMPP.Models.AddProfile;
using COMPP.Services;
using COMPP.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace COMPP.ViewModels
{
    public class AddProfilePictureViewModel : INotifyPropertyChanged
    {
        public Command PictureCommand { get; }
        public Command AddProfile { get; }
        public event PropertyChangedEventHandler PropertyChanged;

        private string base64Image = "";

        ImageSource _imageSource = "dummy_profile4x.png";
        public ImageSource ImageSource
        {
            get { return _imageSource; }
            set
            {
                if (_imageSource != value)
                {
                    _imageSource = value;
                    OnPropertyChanged(nameof(ImageSource));
                }
            }
        }
        public AddProfilePictureViewModel()
        {
            PictureCommand = new Command(async () => await OnPictureSelectClicked());
            AddProfile = new Command(async () => await OnNextClicked());
        }

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private async Task OnPictureSelectClicked()
        {
            var result = await MediaPicker.PickPhotoAsync(new MediaPickerOptions
            {
                Title = "Select a profile picture"
            });

            if (result != null)
            {
                var stream = await result.OpenReadAsync();

                var bytes = new byte[stream.Length];
                await stream.ReadAsync(bytes, 0, (int)stream.Length);
                base64Image = Convert.ToBase64String(bytes);

                ImageSource = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(base64Image)));

            }
        }

        private async Task OnNextClicked()
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));

            EducationRequestModel[] eduInfo = {new EducationRequestModel()
            {
                accountID = id,
                schoolName = await SecureStorage.GetAsync("school_name"),
                startDate = DateTime.Parse(await SecureStorage.GetAsync("edu_start_date")),
                endDate = DateTime.Parse(await SecureStorage.GetAsync("edu_end_date")),
                description = await SecureStorage.GetAsync("jurusan")
            } };

            ExperienceRequestModel[] expInfo = {new ExperienceRequestModel() 
            {
                accountID = id,
                experienceName = await SecureStorage.GetAsync("company_name"),
                startDate = DateTime.Parse(await SecureStorage.GetAsync("exp_start_date")),
                endDate = DateTime.Parse(await SecureStorage.GetAsync("exp_end_date")),
                title = await SecureStorage.GetAsync("position")
            } };

            AddProfileRequestModel profileInfo = new AddProfileRequestModel()
            {
                name = await SecureStorage.GetAsync("name"),
                gender = int.Parse(await SecureStorage.GetAsync("gender")),
                city = await SecureStorage.GetAsync("kota"),
                picture = base64Image,
                dateOfBirth = DateTime.Parse(await SecureStorage.GetAsync("dob")),
                description = await SecureStorage.GetAsync("deskripsi"),
                educations = eduInfo,
                experiences = expInfo
            };

            await AccountService.ServiceClientInstance.AddProfile(id, profileInfo);
            await Shell.Current.GoToAsync("//home");
        }
    }
}
