using COMPP.Services;
using COMPP.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace COMPP.ViewModels
{
    internal class DeleteProfileViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand LogoutCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }

        public DeleteProfileViewModel()
        {
            LogoutCommand = new Command(async() => await OnLogoutClicked());
            DeleteCommand = new Command(async () => await OnDeleteClicked());
        }

        private async Task OnLogoutClicked()
        {
            AccountService.ServiceClientInstance.Logout();
            Application.Current.MainPage = new AppShell();
            await Shell.Current.GoToAsync("//login");
        }

        private async Task OnDeleteClicked()
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            SecureStorage.RemoveAll();
            await AccountService.ServiceClientInstance.DeleteAccount(id);
            Application.Current.MainPage = new AppShell();
        }
    }
}
