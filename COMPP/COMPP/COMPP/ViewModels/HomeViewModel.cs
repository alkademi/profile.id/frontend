using COMPP.Services;
using COMPP.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.CommunityToolkit.Extensions;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace COMPP.ViewModels
{
    internal class HomeViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand SearchCommand { get; private set; }
        public ICommand SettingCommand { get; private set; }
        public ICommand ProfileCommand { get; }

        string _fullname;
        ImageSource _picture;

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public string FullName
        {
            get { return _fullname; }
            set
            {
                if (_fullname != value)
                {
                    _fullname = value;
                    OnPropertyChanged(nameof(FullName));
                }
            }
        }

        public ImageSource Picture
        {
            get { return _picture; }
            set
            {
                if (_picture != value)
                {
                    _picture = value;
                    OnPropertyChanged(nameof(Picture));
                }
            }
        }

        bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                if (_isLoading != value)
                {
                    _isLoading = value;
                    OnPropertyChanged(nameof(IsLoading));
                }
            }
        }

        public HomeViewModel()
        {
            SearchCommand = new Command<string>(async (string query) => await OnSearchClicked(query));
            SettingCommand = new Command(async () => await Shell.Current.GoToAsync("delete-profile"));
            ProfileCommand = new Command(async () => await Shell.Current.GoToAsync("profile"));
        }

        public async Task SetProfile()
        {
            IsLoading = true;

            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            var response = await AccountService.ServiceClientInstance.GetProfile(id);

            if(response != null)
            {
                if (!String.IsNullOrWhiteSpace(response.picture))
                {
                    try
                    {
                        byte[] Base64Stream = Convert.FromBase64String(response.picture);
                        Picture = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                    } catch (Exception ex)
                    {
                        Picture = ImageSource.FromFile("dummy_profile4x.png");
                    }
                } else
                {
                    Picture = ImageSource.FromFile("dummy_profile4x.png");
                }
                if (!String.IsNullOrWhiteSpace(response.name))
                {
                    FullName = response.name;
                }
            }



            IsLoading = false;
        }

        private async Task OnSearchClicked(string query)
        {
            try
            {
                IsLoading = true;
                await Application.Current.MainPage.Navigation.PushAsync(new SearchPage(query), true);
            } catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("Error", ex.ToString(), "OK");
            }
            finally
            {
                IsLoading = false;
            }
        }
    }
}
