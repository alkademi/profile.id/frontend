﻿using COMPP.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace COMPP.ViewModels
{
    public class AddProfileGenderViewModel : INotifyPropertyChanged
    {

        string _btnLTextColor = "White";
        public string BtnLTextColor
        {
            get { return _btnLTextColor; }
            set
            {
                if (_btnLTextColor != value)
                {
                    _btnLTextColor = value;
                    OnPropertyChanged(nameof(BtnLTextColor));
                }
            }
        }

        string _btnLColor = "#47B2BA";
        public string BtnLColor
        {
            get { return _btnLColor; }
            set
            {
                if (_btnLColor != value)
                {
                    _btnLColor = value;
                    OnPropertyChanged(nameof(BtnLColor));
                }
            }
        }

        string _btnPTextColor = "#ADADAD";
        public string BtnPTextColor
        {
            get { return _btnPTextColor; }
            set
            {
                if (_btnPTextColor != value)
                {
                    _btnPTextColor = value;
                    OnPropertyChanged(nameof(BtnPTextColor));
                }
            }
        }

        string _btnPColor = "#E4E4E4";
        public string BtnPColor
        {
            get { return _btnPColor; }
            set
            {
                if (_btnPColor != value)
                {
                    _btnPColor = value;
                    OnPropertyChanged(nameof(BtnPColor));
                }
            }
        }

        int _gender = 1;
        public int Gender
        {
            get { return _gender; }
            set
            {
                if (_gender != value)
                {
                    _gender = value;
                    OnPropertyChanged(nameof(_gender));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand DoB { get; }
        public ICommand BtnLClick { get; }
        public ICommand BtnPClick { get; }    

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public AddProfileGenderViewModel()
        {
            BtnLClick = new Command(async () => await OnBtnLClicked());
            BtnPClick = new Command(async () => await OnBtnPClicked());
            DoB = new Command(async () => await OnNextClicked());

        }

        private async Task OnBtnLClicked()
        {
            Gender = 1;
            BtnLTextColor = "White";
            BtnLColor = "#47B2BA";

            BtnPTextColor = "#ADADAD";
            BtnPColor = "#E4E4E4";
        }

        private async Task OnBtnPClicked()
        {
            Gender = 2;
            BtnPTextColor = "White";
            BtnPColor = "#47B2BA";

            BtnLTextColor = "#ADADAD";
            BtnLColor = "#E4E4E4";
        }

        private async Task OnNextClicked()
        {
            await SecureStorage.SetAsync("gender", Gender.ToString());
            await Shell.Current.GoToAsync("dob");
        }
    }
}
