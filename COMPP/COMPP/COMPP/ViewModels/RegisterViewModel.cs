﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.ComponentModel;
using COMPP.Services;
using COMPP.Views;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace COMPP.ViewModels
{
    public class RegisterViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand RegisterCommand { get; private set; }
        public ICommand Login { get; private set; }

        public RegisterViewModel()
        {
            var token = Task.Run(() => SecureStorage.GetAsync("token")).Result;
            if (token != null)
            {
                Task.Run(() => Shell.Current.GoToAsync("//home")).Wait();
            }
            RegisterCommand = new Command(async () => await OnRegisterClicked());
            Login = new Command(async () => await OnLoginClicked());

        }

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged(nameof(Name));
                }
            }
        }

        string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                if (_email != value)
                {
                    _email = value;
                    OnPropertyChanged(nameof(Email));
                }
            }
        }

        string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                if (_password!= value)
                {
                    _password= value;
                    OnPropertyChanged(nameof(Password));
                }
            }
        }

        bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                if (_isLoading != value)
                {
                    _isLoading = value;
                    OnPropertyChanged(nameof(IsLoading));
                }
            }
        }

        private async Task OnRegisterClicked()
        {
            var content = await AccountService.ServiceClientInstance.Register(Email, Name, Password);

            if (content != null)
            {
                var accountID = content.accountID.ToString();
                if (string.IsNullOrEmpty(accountID))
                {
                    await App.Current.MainPage.DisplayAlert("Alert", "Something went wrong", "Ok");
                }
                else
                {
                    IsLoading = true;
                    var token = await AccountService.ServiceClientInstance.Login(Email, Password);
                    if (token == null)
                    {
                        IsLoading= false;
                        await App.Current.MainPage.DisplayAlert("Error", "Login failed", "OK");
                    }
                    else
                    {
                        await SecureStorage.SetAsync("account_id", accountID);
                        await SecureStorage.SetAsync("name", Name);
                        await SecureStorage.SetAsync("email", content.email);
                        await SecureStorage.SetAsync("gender", content.gender.ToString());

                        IsLoading = false;
                        await Shell.Current.GoToAsync("//gender");
                    }

                }
            }

            else
            {
                await App.Current.MainPage.DisplayAlert("Alert", "Something went wrong", "Ok");
            }

        }

        private async Task OnLoginClicked()
        {
            await Shell.Current.GoToAsync("//register/login");
        }
    }
}