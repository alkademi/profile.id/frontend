﻿using System;
using Xamarin.Forms;
using COMPP.Services;
using COMPP.Models.AddProfile;
using COMPP.Models;
using System.Collections.Generic;
using System.Windows.Input;
using System.Threading.Tasks;
using Xamarin.Essentials;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;

namespace COMPP.ViewModels
{
    public class EditAddProfileEducationViewModel : INotifyPropertyChanged
    {

        // init
        public ICommand EditEducation { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        public EditAddProfileEducationViewModel()
        {
            EditEducation = new Command(async () => await OnSaveClicked());

        }

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        string _schoolName = "";
        public string SchoolName
        {
            get { return _schoolName; }
            set
            {
                if (_schoolName != value)
                {
                    _schoolName = value;
                    OnPropertyChanged(nameof(SchoolName));
                }
            }
        }

        string _jurusan = "";
        public string Jurusan
        {
            get { return _jurusan; }
            set
            {
                if (_jurusan != value)
                {
                    _jurusan = value;
                    OnPropertyChanged(nameof(Jurusan));
                }
            }
        }

        DateTime _eduStartDate = DateTime.Today;
        public DateTime EduStartDate
        {
            get { return _eduStartDate; }
            set
            {
                if (_eduStartDate != value)
                {
                    _eduStartDate = value;
                    OnPropertyChanged(nameof(EduStartDate));
                }
            }
        }

        DateTime _eduEndDate = DateTime.Today;

        public DateTime EduEndDate
        {
            get { return _eduEndDate; }
            set
            {
                if (_eduEndDate != value)
                {
                    _eduEndDate = value;
                    OnPropertyChanged(nameof(EduEndDate));
                }
            }
        }

        private async Task OnSaveClicked()
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            EducationRequestModel eduInfo = new EducationRequestModel()
            {
                accountID = id,
                schoolName = SchoolName,
                startDate = EduStartDate,
                endDate = EduEndDate,
                description = Jurusan
            };
            await AccountService.ServiceClientInstance.AddEducation(eduInfo);
            await Shell.Current.GoToAsync("..");
        }

    }
}

