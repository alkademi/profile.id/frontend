﻿using COMPP.Views;
using System;
using COMPP.Models.AddProfile;
using Xamarin.Forms;
using System.Windows.Input;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Essentials;
using COMPP.Services;

namespace COMPP.ViewModels
{
    public class EditProfileExperienceViewModel : INotifyPropertyChanged
    {
        public ICommand EditExperience { get; }
        public ICommand DeleteExperience { get; }

        public event PropertyChangedEventHandler PropertyChanged;
        public EditProfileExperienceViewModel(int experienceID)
        {
            GetExperiences(experienceID);
            EditExperience = new Command(async () => await OnSaveClicked(experienceID));
            DeleteExperience = new Command(async () => await OnDeleteClicked(experienceID));
        }

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        string _companyName;
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                if (_companyName != value)
                {
                    _companyName = value;
                    OnPropertyChanged(nameof(CompanyName));
                }
            }
        }

        string _position;
        public string Position
        {
            get { return _position; }
            set
            {
                if (_position != value)
                {
                    _position = value;
                    OnPropertyChanged(nameof(Position));
                }
            }
        }

        DateTime _expStartDate;
        public DateTime ExpStartDate
        {
            get { return _expStartDate; }
            set
            {
                if (_expStartDate != value)
                {
                    _expStartDate = value;
                    OnPropertyChanged(nameof(ExpStartDate));
                }
            }
        }

        DateTime _expEndDate;

        public DateTime ExpEndDate
        {
            get { return _expEndDate; }
            set
            {
                if (_expEndDate != value)
                {
                    _expEndDate = value;
                    OnPropertyChanged(nameof(ExpEndDate));
                }
            }
        }

        private async void GetExperiences(int experienceID)
        {
            var response = await AccountService.ServiceClientInstance.GetExperiences(experienceID);

            if (response == null)
            {
            }
            else
            {
                CompanyName = response.experienceName;
                ExpStartDate = response.startDate;
                ExpEndDate = response.endDate;
                Position = response.title;
            }
        }

        private async Task OnSaveClicked(int experienceID)
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            ExperienceRequestModel expInfo = new ExperienceRequestModel()
            {
                accountID = id,
                experienceName = CompanyName,
                startDate = ExpStartDate,
                endDate = ExpEndDate,
                title = Position,
            };
            await AccountService.ServiceClientInstance.EditExperience(experienceID, expInfo);
            await Shell.Current.GoToAsync("..");
        }

        private async Task OnDeleteClicked(int experienceID)
        {
            await AccountService.ServiceClientInstance.DeleteExperience(experienceID);
            await Shell.Current.GoToAsync("..");
        }
    }
}
