﻿using System;

using Xamarin.Forms;
using COMPP.Services;
using COMPP.Models.EditProfile;
using System.Collections.Generic;
using System.Windows.Input;
using System.Threading.Tasks;
using Xamarin.Essentials;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace COMPP.ViewModels
{
    public class EditProfileAboutViewModel : INotifyPropertyChanged
    {

        public ICommand EditAbout { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        public EditProfileAboutViewModel()
        {
            GetIdentity();
            EditAbout = new Command(async () => await OnSaveClicked());
        }

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        string _name;
        string _description;
        string _location;
        DateTime _date;
        string Email;
        int Gender;
        string base64Image="";

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged(nameof(Name));
                }
            }
        }

        public string About
        {
            get { return _description; }
            set
            {
                if (_description != value)
                {
                    _description = value;
                    OnPropertyChanged(nameof(About));
                }
            }
        }

        public string Location
        {
            get { return _location; }
            set
            {
                if (_location != value)
                {
                    _location = value;
                    OnPropertyChanged(nameof(Location));
                }
            }
        }

        public DateTime Date
        {
            get { return _date; }
            set
            {
                if (_date != value)
                {
                    _date = value;
                    OnPropertyChanged(nameof(Date));
                }
            }
        }

        ImageSource _imageSource = "dummy_profile4x.png";
        public ImageSource ImageSource
        {
            get { return _imageSource; }
            set
            {
                if (_imageSource != value)
                {
                    _imageSource = value;
                    OnPropertyChanged(nameof(ImageSource));
                }
            }
        }


        private async void GetIdentity()
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            var response = await AccountService.ServiceClientInstance.GetProfile(id);

            if (response == null)
            {
            }
            else
            {
                Name = response.name;
                About = response.description;
                Location = response.city;
                Date = response.dateOfBirth;
                base64Image = response.picture;
            }
        }

        private async Task OnSaveClicked()
        {

            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            EditProfileRequestModel profileInfo = new EditProfileRequestModel()
            {
                gender = int.Parse(await SecureStorage.GetAsync("gender")),
                email = await SecureStorage.GetAsync("email"),
                name = Name,
                city = Location,
                picture = base64Image,
                dateOfBirth = Date,
                description = About
            };

            await AccountService.ServiceClientInstance.EditProfileIdentity(id, profileInfo);
            await Shell.Current.GoToAsync("..");
        }

    }
}

