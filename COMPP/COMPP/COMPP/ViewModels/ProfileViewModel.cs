﻿using COMPP.Views;
using COMPP.Models;
using COMPP.Models.Connection;
using COMPP.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Linq;
using Xamarin.Essentials;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.IO;

namespace COMPP.ViewModels
{
    public class ProfileViewModel : INotifyPropertyChanged
    {
        public ICommand LoadMoreExperience { get; private set; }
        public ICommand LoadMoreEducation { get; private set; }
        public ICommand Back { get; private set; }
        public ICommand Identity { get; private set; }
        public ICommand Experience { get; private set; }
        public ICommand AboutCommand { get; private set; }
        public ICommand ConnectionCommand { get; private set; }

        private string base64Image = "";
        string _name;
        string _description;
        string _location;
        string _about;


        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged(nameof(Name));
                }
            }
        }

        string _email;
        public string Email 
        { 
            get { return _email; } 
            set
            {
                if (_email != value)
                {
                    _email = value;
                    OnPropertyChanged(nameof(Email));
                }
            }
        }

        public string About
        {
            get { return _about; }
            set
            {
                if (_about != value)
                {
                    _about = value;
                    OnPropertyChanged(nameof(About));
                }
            }
        }

        public string Location
        {
            get { return _location; }
            set
            {
                if (_location != value)
                {
                    _location = value;
                    OnPropertyChanged(nameof(Location));
                }
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (_description != value)
                {
                    _description = value;
                    OnPropertyChanged(nameof(Description));
                }
            }
        }

        ImageSource _imageSource = "dummy_profile4x.png";
        public ImageSource Picture
        {
            get { return _imageSource; }
            set
            {
                if (_imageSource != value)
                {
                    _imageSource = value;
                    OnPropertyChanged(nameof(Picture));
                }
            }
        }


        //init
        public ObservableCollection<Experience> ExperienceList { get; set; } = new ObservableCollection<Experience>();
        public ObservableCollection<Experience> ExperienceListShow { get; set; } = new ObservableCollection<Experience>();

        public ObservableCollection<Education> EducationList { get; set; } = new ObservableCollection<Education>();

        public ObservableCollection<Education> EducationListShow { get; set; } = new ObservableCollection<Education>();

        private async Task GetIdentity()
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            var response = await AccountService.ServiceClientInstance.GetProfile(id);

            if (response == null)
            {
            }
            else
            {
                Email = response.email;
                Name = response.name;
                About = response.description;
                Location = response.city;
                base64Image = response.picture;

                if (!string.IsNullOrWhiteSpace(base64Image))
                {
                    try
                    {
                        byte[] Base64Stream = Convert.FromBase64String(base64Image);
                        Picture = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                    }
                    catch (Exception ex)
                    {
                        Picture = ImageSource.FromFile("dummy_profile4x.png");
                    }
                }
                else
                {
                    Picture = ImageSource.FromFile("dummy_profile4x.png");
                }

                Description = "";
                if (response.educations.Count > 0)
                {
                    Description = response.educations[response.educations.Count - 1].description + " - " + response.educations[response.educations.Count - 1].schoolName;
                    if (response.experiences.Count > 0)
                    {
                        Description = Description + " | " + response.experiences[response.experiences.Count - 1].title + " - " + response.experiences[response.experiences.Count - 1].experienceName;
                    }
                }
                else
                {
                    if (response.experiences.Count > 0)
                    {
                        Description = Description + response.experiences[response.experiences.Count - 1].title + " - " + response.experiences[response.experiences.Count - 1].experienceName;
                    }
                }
            }
        }

        private async Task GetEducationList()
        {
            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            var response = await AccountService.ServiceClientInstance.GetProfile(id);

            if (response == null)
            {

            }
            else
            {
                EducationList.Clear();
                for (var i = 0; i < response.educations.Count; i++)
                {
                    EducationList.Add(new Education(response.educations[i].educationID, response.educations[i].accountID, response.educations[i].schoolName, response.educations[i].startDate, response.educations[i].endDate, response.educations[i].description, "icon_school.png"));
                }
                if (EducationList.Count > 0)
                {
                    EducationListShow = new ObservableCollection<Education> {
                EducationList.ElementAt(0),
                };
                    EducationListShow[0] = EducationList.ElementAt(0);
                }
                else
                {
                    EducationListShow.Clear();
                }
                if (EducationList.Count > 1)
                {
                    LoadMoreStateEducation = true;
                }
                else
                {
                    LoadMoreStateEducation = false;
                }
                OnPropertyChanged("EducationList");
                OnPropertyChanged("EducationHeight");
                OnPropertyChanged("EducationListShow");
                OnPropertyChanged("LoadMoreStateEducation");
            }
        }

        private async Task GetExperienceList()
        {

            int id = int.Parse(await SecureStorage.GetAsync("account_id"));
            var response = await AccountService.ServiceClientInstance.GetProfile(id);

            if (response == null)
            {

            }
            else
            {
                ExperienceList.Clear();
                for (var i = 0; i < response.experiences.Count; i++)
                {
                    ExperienceList.Add(new Experience(response.experiences[i].experienceID, response.experiences[i].accountID, response.experiences[i].experienceName, response.experiences[i].startDate, response.experiences[i].endDate, response.experiences[i].title, response.experiences[i].description, "icon_company.png"));
                }
                int max = 3;
                if (ExperienceList.Count < max)
                {
                    max = ExperienceList.Count;
                }
                if (ExperienceList.Count > 0)
                {
                    ExperienceListShow = new ObservableCollection<Experience> {
                    ExperienceList.ElementAt(0),
                 };
                    for (var i = 1; i < max; i++)
                    {
                        ExperienceListShow.Add(ExperienceList.ElementAt(i));
                    }
                }
                else
                {
                    ExperienceListShow.Clear();
                }
                if (ExperienceList.Count > 3)
                {
                    LoadMoreStateExperience = true;
                }
                else
                {
                    LoadMoreStateExperience = false;
                }
                OnPropertyChanged("ExperienceList");
                OnPropertyChanged("ExperienceHeight");
                OnPropertyChanged("ExperienceListShow");
                OnPropertyChanged("LoadMoreStateExperience");
            }
        }


        public int ExperienceHeight
        {
            get => (ExperienceListShow.Count) * 72;
        }

        public int EducationHeight
        {
            get => (EducationListShow.Count) * 72;
        }

        public bool LoadMoreStateExperience { get; set; }

        public bool LoadMoreStateEducation { get; set; }

        public int ConnectionCount { get; set; }

        bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                if (_isLoading != value)
                {
                    _isLoading = value;
                    OnPropertyChanged(nameof(IsLoading));
                }
            }
        }

        private async Task GetConnectionList()
        {
            var account_id = int.Parse(await SecureStorage.GetAsync("account_id"));
            var result = await ConnectionService.ServiceClientInstance.GetConnectionList(account_id);
            if (result == null)
            {
                ConnectionCount = 0;
            }
            else
            {
                ConnectionCount = result.Count;
            }
            OnPropertyChanged("ConnectionCount");
        }


        public ProfileViewModel() {
            LoadMoreExperience = new Command(async () => await OnLoadMoreExperience());
            LoadMoreEducation = new Command(async () => await OnLoadMoreEducation());
            Back = new Command(async () => await Shell.Current.GoToAsync(".."));
            Identity = new Command(async () => await Shell.Current.GoToAsync("edit-identity"));
            Experience = new Command(async () => await Shell.Current.GoToAsync("edit-career"));
            AboutCommand= new Command(async () => await Shell.Current.GoToAsync("edit-about"));
            ConnectionCommand = new Command(async () => await OnConnectionInfoClicked());
        }

        public async Task loadData()
        {
            await GetIdentity();
            await GetExperienceList();
            await GetEducationList();
            await GetConnectionList();
        }

        public async Task OnLoadMoreExperience()
        {
            await Task.Run(() => ExperienceListShow = ExperienceList);
            LoadMoreStateExperience = false;
            OnPropertyChanged("LoadMoreStateExperience");
            OnPropertyChanged("ExperienceListShow");
            OnPropertyChanged("ExperienceHeight");
        }

        public async Task OnLoadMoreEducation()
        {
            await Task.Run(() => EducationListShow = EducationList);
            LoadMoreStateEducation = false;
            OnPropertyChanged("LoadMoreStateEducation");
            OnPropertyChanged("EducationListShow");
            OnPropertyChanged("EducationHeight");
        }

        public async Task OnConnectionInfoClicked()
        {
            IsLoading = true; 
            var ID = int.Parse(await SecureStorage.GetAsync("account_id"));
            await Application.Current.MainPage.Navigation.PushAsync(new ConnectionPage(ID), true);
            IsLoading = false;
        }
    }
}
