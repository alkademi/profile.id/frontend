﻿using COMPP.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using System.ComponentModel;
using Xamarin.Essentials;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace COMPP.ViewModels
{
    public class AddProfileDobViewModel : INotifyPropertyChanged
    {
        public ICommand Education { get; }
        public event PropertyChangedEventHandler PropertyChanged;

        public AddProfileDobViewModel()
        {
            Education = new Command(async () => await OnNextClicked());
        }

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        string _kota = "";
        public string Kota
        {
            get { return _kota; }
            set
            {
                if (_kota != value)
                {
                    _kota = value;
                    OnPropertyChanged(nameof(Kota));
                }
            }
        }

        string _deskripsi = "";
        public string Deskripsi
        {
            get { return _deskripsi; }
            set
            {
                if (_deskripsi != value)
                {
                    _deskripsi = value;
                    OnPropertyChanged(nameof(Deskripsi));
                }
            }
        }

        DateTime _dateOfBirth = DateTime.Today;
        public DateTime DateOfBirth
        {
            get { return _dateOfBirth; }
            set
            {
                if (_dateOfBirth != value)
                {
                    _dateOfBirth = value;
                    OnPropertyChanged(nameof(DateOfBirth));
                }
            }
        }

        private async Task OnNextClicked()
        {
            await SecureStorage.SetAsync("dob", DateOfBirth.ToString());
            await SecureStorage.SetAsync("kota", Kota);
            await SecureStorage.SetAsync("deskripsi", Deskripsi);
            await Shell.Current.GoToAsync("education");
        }
    }
}
