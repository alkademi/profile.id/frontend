﻿using COMPP.Models.Connection;
using COMPP.Models.Search;
using COMPP.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
namespace COMPP.ViewModels
{
    internal class ConnectionViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand SearchCommand { get; private set; }

        private int _accountID;

        private string _searchQuery;
        public string SearchQuery
        {
            get { return _searchQuery; }
            set
            {
                if (_searchQuery != value)
                {
                    _searchQuery = value;
                    OnPropertyChanged(nameof(_searchQuery));
                }
            }
        }

        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsRefreshing = true;

                    Result = await GetConnectionList(_accountID);
                    UiResult = Result;

                    IsRefreshing = false;
                });
            }
        }

        bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                if (_isLoading != value)
                {
                    _isLoading = value;
                    OnPropertyChanged(nameof(IsLoading));
                }
            }
        }

        private ObservableCollection<ConnectionListItem> _result;
        public ObservableCollection<ConnectionListItem> Result
        {
            get => _result;
            set
            {
                if (value != _result)
                {
                    _result = value;
                    OnPropertyChanged(nameof(Result));
                }
            }
        }

        private ObservableCollection<ConnectionListItem> _uiResult;
        public ObservableCollection<ConnectionListItem> UiResult
        {
            get => _uiResult;
            set
            {
                if (value != _uiResult)
                {
                    _uiResult = value;
                    OnPropertyChanged(nameof(UiResult));
                }
            }
        }

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ConnectionViewModel(int accountID)
        {
            _accountID = accountID;
            SearchCommand = new Command<string>((string searchQuery) => OnSearchClicked(searchQuery));
            Result = Task.Run(async () => await GetConnectionList(accountID)).Result;
            UiResult = Result;
        }
        private async Task<ObservableCollection<ConnectionListItem>> GetConnectionList(int accountID)
        {
            IsLoading = true;
            var result = await ConnectionService.ServiceClientInstance.GetConnectionList(accountID);

            var uiResult = new ObservableCollection<ConnectionListItem>();

            result.ForEach(item =>
            {
                if (item.ProfileInfo.Picture != null && item.ProfileInfo.Picture != "")
                {
                    try
                    {
                        byte[] Base64Stream = Convert.FromBase64String(item.ProfileInfo.Picture);
                        item.ProfileInfo.pictureObj = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                    } 
                    catch (Exception ex)
                    {
                        item.ProfileInfo.pictureObj = ImageSource.FromFile("dummy_profile4x.png");
                    }
                }
                else
                {
                    item.ProfileInfo.pictureObj = ImageSource.FromFile("dummy_profile4x.png");
                }

                var connectionItem = new ConnectionListItem
                {

                    ConnectionID = item.ConnectionID,
                    Id = item.ProfileInfo.Id,
                    Name = item.ProfileInfo.Name,
                    Gender = item.ProfileInfo.Gender,
                    City = item.ProfileInfo.City,
                    Picture = item.ProfileInfo.Picture,
                    pictureObj = item.ProfileInfo.pictureObj,
                    DateOfBirth = item.ProfileInfo.DateOfBirth,
                    Description = item.ProfileInfo.Description,
                    LastEducation = item.ProfileInfo.Educations.DefaultIfEmpty(
                        new Models.EducationResponseModel { 
                            description = "No education history"
                        }
                        ).Last().description,
                    LastExperience = item.ProfileInfo.Experiences.DefaultIfEmpty(
                        new Models.ExperienceResponseModel
                        {
                            description = "No experience history"
                        }
                        ).Last().description,
                };

                uiResult.Add(connectionItem);
            });

            var convertedResult = new ObservableCollection<ConnectionListItem>(uiResult);

            IsLoading = false;

            return convertedResult;
        }

        private ObservableCollection<ConnectionListItem> GetSearchResult(string query)
        {
            var result = new List<ConnectionListItem>();
            
            if(String.IsNullOrWhiteSpace(query))
            {
                return Result;
            } 
            else
            {
                foreach (var item in Result)
                {
                    if (item.Name.Contains(query)){
                        result.Add(item);
                    }
                }

                var convertedResult = new ObservableCollection<ConnectionListItem>(result);

                return convertedResult;
            }

        }

        private void OnSearchClicked(string query)
        {
            SearchQuery = query;
            UiResult = GetSearchResult(query);
        }
    }
}
